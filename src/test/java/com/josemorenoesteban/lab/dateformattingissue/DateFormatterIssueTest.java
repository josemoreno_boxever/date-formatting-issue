// Copyright 2020 José Moreno. All rights reserved.
// Use of this source code is governed by a DO WHAT THE FUCK YOU WANT TO
// license that can be found in the LICENSE file.

package com.josemorenoesteban.lab.dateformattingissue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/*
 * This test shows a possible unexpected behaviour with dates when we 
 * format dates with java.time.format.DateTimeFormatter.
 * 
 * @see https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
 */
public class DateFormatterIssueTest {

    /*
     * Using the 'y' pattern everything works as expected, because we format the "year of era"
     */
    @Test 
    public void checkOk() {
        final String    expected = "2019/12/30";
        final LocalDate date     = LocalDate.of(2019, 12, 30);

        String formattedDate = date.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        assertEquals(expected, formattedDate);
    }

    /*
     * Using the 'Y' pattern we format year with "week based year", so in this case 
     * "December the 31th of 2019" is "2020", so it could be an unexpected behaviour.
     */
    @Test 
    public void checkFailure() {
        final String    expected = "2019/12/30";
        final LocalDate date     = LocalDate.of(2019, 12, 30);

        String formattedDate = date.format(DateTimeFormatter.ofPattern("YYYY/MM/dd")); 
        // "YYYY" is week based year, so the formattedDate is "2020/12/30" not "2019/12/30" as expected
        assertNotEquals(expected, formattedDate);
    }
}
