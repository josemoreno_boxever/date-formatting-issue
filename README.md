# Date formating issue

This project is a proof of concept about a possible *issue* with
Java Date formatting templates.

You can see an explanation in the [unit test](./src/test/java/com/josemorenoesteban/lab/dateformattingissue/DateFormatterIssueTest.java).

## Requisites

- Java 8 SDK, as programming language.
- Maven, as build automation.
- [Optional], as build automation tool.

## Running the project

Project must build sucessfully with two test passed without errors.

```bash
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running com.josemorenoesteban.lab.dateformattingissue.DateFormatterIssueTest
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.116 sec

Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
```

## TL;DR

Check requisites and:

```bash
git clone git@bitbucket.org:josemoreno_boxever/date-formatting-issue.git && cd date-formatting-issue && make
```

### Run with make

The default `make` rule will run the test.

```bash
make
```

### Run with Maven

```bash
mvn clean test
```
